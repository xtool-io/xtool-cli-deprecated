package xtool.cli.service

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.shell.jline.InteractiveShellApplicationRunner
import org.springframework.shell.jline.ScriptShellApplicationRunner
import xtool.cli.XtoolCliApplication


@EnableAutoConfiguration
@SpringBootTest(classes = [XtoolCliApplication::class],
        properties = [
            "${InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED}=false",
            "${ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED}=false"
        ])
class XtoolServiceTestCase @Autowired constructor(private val service: XtoolGeneratePkgService) {

    @Test
    fun test() {
//        service.run(XtoolParam("spring", "create", "package"))//
//        TODO("Write a test!")
    }
}
