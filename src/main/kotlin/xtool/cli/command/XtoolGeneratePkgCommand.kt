package xtool.cli.command

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import xtool.cli.service.XtoolGeneratePkgService
import xtool.core.XtoolCoreModule

/**
 * Comando de geração de pacotes xtool.
 */
@ShellComponent
class XtoolGeneratePkgCommand(val service: XtoolGeneratePkgService) {

    @ShellMethod(key = ["xtool:generate/pkg"], value = "Gera um pacote xtool", group = XtoolCoreModule.XTOOL_DEFAULT_GROUP)
    fun run(@ShellOption(value = ["--namespace"]) namespace: String,
            @ShellOption(value = ["--action"]) action: String,
            @ShellOption(value = ["--artifact"]) artifact: String,
            @ShellOption(value = ["--description"]) description: String) = service.run(namespace, action, artifact, description)

}
