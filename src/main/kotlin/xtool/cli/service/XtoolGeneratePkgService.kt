package xtool.cli.service

import org.slf4j.Logger
import org.springframework.stereotype.Service
import xtool.core.api.FS
import xtool.core.api.Workspace

@Service
class XtoolGeneratePkgService(private val fs: FS,
                              private val log: Logger,
                              private val workspace: Workspace) {

    fun run(namespace: String, action: String, artifact: String, description: String) {
        log.debug("Workspace: ${workspace.path}")
        val projectName = "${namespace}-${action}-${artifact}"
        val projectPath = fs.mkdirs(workspace.path.resolve(projectName))

        val vars = mapOf(
                "projectName" to projectName,
                "namespace" to namespace,
                "action" to action,
                "artifact" to artifact,
                "description" to description)

        fs.copyResources("pkg:v1", projectPath, vars)
    }
}
