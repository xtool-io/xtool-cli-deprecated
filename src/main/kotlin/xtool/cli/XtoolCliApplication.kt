package xtool.cli

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class XtoolCliApplication

fun main(args: Array<String>) {
    runApplication<XtoolCliApplication>(*args)
}
