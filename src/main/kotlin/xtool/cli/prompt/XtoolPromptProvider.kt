package xtool.cli.prompt

import org.jline.utils.AttributedString
import org.jline.utils.AttributedStringBuilder
import org.jline.utils.AttributedStyle
import org.springframework.shell.jline.PromptProvider
import org.springframework.stereotype.Component

@Component
class XtoolPromptProvider : PromptProvider {

    override fun getPrompt(): AttributedString {
        return AttributedStringBuilder()
                .append("\uD83D\uDC7D ")
                .style(AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.WHITE))
                .append(System.getProperty("user.dir").replace(System.getProperty("user.home"), "~"))
                .style(AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.GREEN))
                .append("$ ")
                .toAttributedString();
    }
}
